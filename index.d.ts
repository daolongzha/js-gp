

declare module 'js-gp'{
    /**
     * gp security default enc,mac,dek key
     * 404142434445464748494A4B4C4D4E4F
     */
    export const GP_DEFAULT_KEY: Buffer;

    /**
     * calculate mac default icv
     * 0000000000000000
     */
    export const GP_DEFAULT_ICV: Buffer;

    /**
     * gp security level
     */
    export enum GP_SECURITY_LEVEL {
        NO_SECURITY = 0,
        COMMAND_MAC = 1,
        COMMAND_MAC_EN = 3
    }

    /**
     * interface for transmit apdu
     */
    export interface IApdu {
        transmit(command: Buffer): Promise<Buffer>;
    }
    
    /**
     * key information for extern authentication
     */
    export interface gp_key_info {
        key_enc: Buffer;
        key_mac: Buffer;
        key_dek: Buffer;
        key_ver: number;
    }
    
    /**
     * a const default key information
     */
    export const defaultkey : gp_key_info;
    
    /**
     * gp authentication protocol
     * current only support SCP_02
     */
    export enum GP_PROTOCOL {
        SCP_01 = 0,
        SCP_02 = 1
    }

    /**
     * all implementation class
     */
    export class jsgp{
        /**
         * 
         * @param ap implementation the transmit interface IApdu
         */
        constructor(ap: IApdu);

        /**
         * impl gp initupdate and extern authentication use protocol scp-01
         * @param security_level gp authentication security level
         * @param key authentication key information
         */
        public gp_security_ch_01(security_level: number, key: gp_key_info): Promise<Buffer>;
        /**
         * impl gp initupdate and extern authentication use protocol scp-02
         * @param security_level gp authentication security level
         * @param key authentication key information
         */
        public gp_security_ch_02(security_level: number, key?: gp_key_info): Promise<Buffer>;

        /**
         * gp select command
         * @param p1p2 command p1p2 parameter
         * @param name fid or aid
         */
        public gp_select(p1p2: number, name: Buffer): Promise<Buffer>;

        /**
         * select file by aid p1p2 = 0400
         * @param aid file aid
         */
        public gp_select_aid(aid: Buffer): Promise<Buffer>;

        /**
         * gp delete content
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_delete(p1p2: number, data: Buffer): Promise<Buffer>;
            
        /**
         * gp delete by aid, command field = 4F len aid
         * @param p1p2 command p1p2 paramter
         * @param aid file aid
         */
        public gp_delete_aid(p1p2: number, aid: Buffer): Promise<Buffer>;

        /**
         * gp manage local channel
         * @param p1p2 command p1p2 parameter
         */
        public gp_manage_ch(p1p2: number): Promise<Buffer>;

        /**
         * gp install command impl
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_install(p1p2: number, data: Buffer): Promise<Buffer>;

        /**
         * 
         * @param p1p2 command p1p2 parameter
         * @param aid1 data field aid1
         * @param aid2 data field aid2
         * @param aid3 data field aid3
         * @param pri install priority
         * @param para install parameter
         * @param token install token
         */
        public gp_install_ex(p1p2: number, aid1: Buffer, aid2: Buffer, aid3: Buffer, pri: number, para: Buffer, token: Buffer): Promise<Buffer>;
        
        /**
         * gp load command impl
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_load(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * gp store data impl
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_storedata(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * function auto calc dgi and command
         * @param p1 command p1 parameter
         * @param dgi store data dgi
         * @param dgidata  the dgi data
         */
        public gp_storedgi(p1: number, dgi: number, dgidata: Buffer): Promise<Buffer>;
        
        /**
         * gp put key command
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_putkey(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * gp get data command
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_getdata(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * gp get status command
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_getstatus(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * gp set status command
         * @param p1p2 command p1p2 parameter
         * @param data command data field
         */
        public gp_setstatus(p1p2: number, data: Buffer): Promise<Buffer>;
        
        /**
         * 
         * @param capfile cap file path
         * @return components of the cap file,not include Debug.cap component
         */
        public gp_cap_data(capfile: string): Buffer;
        
        /**
         * 
         * @param capfile cap file path
         * @return the cap file aid of package
         */
        public gp_cap_pkg_aid(capfile: string): Buffer;

        /**
         * use gp-load command to load the input data
         * @param capdata cap file components to load
         * @return last load command result
         */
        public gp_load_cap(capdata: Buffer): Promise<Buffer>;

        /**
         * this function auto calc command mac and encrypt command data
         * then call transmit function return result
         * @param cmd the apdu command
         * @return return the command added with mac or data field encrypted
         */
        public gp_cmd_mac(cmd: Buffer): Promise<Buffer>;     

        /**
         * 
         * @param data calculate input data gp-mac use gp-session-mac key
         * @return 8 bytes mac data
         */
        public gp_data_mac(data: Buffer): Buffer;
        
        /**
         * 
         * @param data encrypt input data use gp-session-enc key
         * @return encrypted data
         */
        public gp_data_encrypt(data: Buffer): Buffer;
        
        /**
         * 
         * @param data encrypt input data use gp-session-dek key
         * @return encrypted data
         */
        public gp_sensitive_encrypt(data: Buffer): Buffer;            

    }
}

